﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PuttyTheme {
    public partial class Form1 : Form {
        public const string putty_path = @"C:\Użytki\Putty";
        public string selected_theme;

        public Form1() {
            InitializeComponent();

            DirectoryInfo d = new DirectoryInfo(putty_path + @"\Themes\");
            FileInfo[] Files = d.GetFiles("*.reg");

            foreach(FileInfo file in Files)
            {
                string theme = file.Name.Replace(".reg", "");
                this.listBox.Items.Add(theme);
            }
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selected_theme = listBox.SelectedItem.ToString();
        }

        private void button_Click(object sender, EventArgs e)
        {
            string[] colours = new string[256];
            string line;

            System.IO.StreamReader theme = new System.IO.StreamReader(putty_path + @"\Themes\" + selected_theme + @".reg");
            while((line = theme.ReadLine()) != null)
            {
                int index;

                Regex r = new Regex(@"Colour(\d+)""=""(.*)""", RegexOptions.IgnoreCase);
                Match m = r.Match(line);
                if(m.Success)
                {
                    int.TryParse(m.Groups[1].Value, out index);
                    colours[index] = m.Groups[2].Value;
                }
            }

            theme.Close();

            DirectoryInfo d = new DirectoryInfo(putty_path + @"\Sessions\");
            FileInfo[] Files = d.GetFiles("*");

            foreach(FileInfo file in Files)
            {
                List<string> buffer = new List<string>();

                System.IO.StreamReader session = new System.IO.StreamReader(putty_path + @"\Sessions\" + file.Name);
                while((line = session.ReadLine()) != null)
                {
                    Regex r = new Regex(@"Colour(\d+)\\(.*)\\", RegexOptions.IgnoreCase);
                    Match m = r.Match(line);
                    if(m.Success)
                    {
                        int index;
                        int.TryParse(m.Groups[1].Value, out index);

                        if (colours[index] == null)
                        {
                            buffer.Add(line);
                        }
                        else
                        {
                            buffer.Add(@"Colour" + m.Groups[1].Value + @"\" + colours[index] + @"\");
                        }
                    }
                    else
                    {
                        buffer.Add(line);
                    }
                }

                session.Close();

                FileStream fs = new FileStream(putty_path + @"\Sessions\" + file.Name, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);

                foreach(string b in buffer)
                {
                    sw.WriteLine(b);
                }

                sw.Close();
                fs.Close();
            }
        }
    }
}
